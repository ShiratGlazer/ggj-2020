﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public RectTransform item_menu_garbage;
    public Camera m_camera;
    public float energy_toll_factor = 10;
    public GameObject TrashPrefab1;
    public GameObject TrashPrefab2;
    public GameObject TrashPrefab3;


    ItemSelect selected_item = null;


    // Start is called before the first frame update
    void Start()
    {

        item_menu_garbage.gameObject.GetComponent<CanvasGroup>().alpha = 0f; //this makes everything transparent
        item_menu_garbage.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void AddGarbage(Vector3 pos)
    {
        GameObject prefab = TrashPrefab1;

        int val = (int)(Random.value * 3);

        if(val == 0)
        {
            prefab = TrashPrefab2;
        }

        if(val == 1)
        {
            prefab = TrashPrefab3;
        }
        

        Instantiate(prefab, pos, new Quaternion(), this.transform);
    }

    public void DeselectMe(ItemSelect to_deselect)
    {
        this.selected_item = null;
        to_deselect.selected = false;
    }

    public void Deselect()
    {
        if(this.selected_item != null)
        {
            this.selected_item.selected = false;
        }
        this.selected_item = null;
        Debug.Log("click");
    }

    public void SelectMe(ItemSelect to_select)
    {
        if (this.selected_item == null)
        {
        }
        else
        {
            selected_item.selected = false;
        }

        this.selected_item = to_select;
        to_select.selected = true;
        to_select.last_selected = Time.time;
    }

    public void ItemBeingHarvested()
    {
        if(selected_item.type=="Garbage")
        {
            GameObject.Find("Player").GetComponent<PlayerScript>().material_amount += selected_item.abs_parent.GetComponent<HealthState>().health_factor * -1;
            GameObject.Find("Player").GetComponent<PlayerScript>().robotEnergy += selected_item.abs_parent.GetComponent<HealthState>().health_factor * energy_toll_factor; // adding negative number

            Destroy(selected_item.abs_parent);
        }
        else
        {
            Debug.Log("How the fuck");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(selected_item != null)
        {
            if(selected_item.type == "Garbage")
            {
                Vector3 org_pos = selected_item.gameObject.transform.position;
                item_menu_garbage.position = Camera.main.WorldToScreenPoint(new Vector3(
                                                                        org_pos.x + 2,
                                                                        org_pos.y + 2,
                                                                        org_pos.z
                                                                    ));
                item_menu_garbage.gameObject.GetComponent<CanvasGroup>().alpha = 1f;
                item_menu_garbage.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;

                float health_factor = selected_item.abs_parent.GetComponent<HealthState>().health_factor * -1;

                item_menu_garbage.GetComponentInChildren<MaterialPanelScript>().material = health_factor;

            }
            else
            {

                item_menu_garbage.gameObject.GetComponent<CanvasGroup>().alpha = 0f; //this makes everything transparent
                item_menu_garbage.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events

            }
        }
        else
        {
            item_menu_garbage.gameObject.GetComponent<CanvasGroup>().alpha = 0f; //this makes everything transparent
            item_menu_garbage.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events
        }
    }
}
