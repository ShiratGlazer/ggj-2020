﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    public UIController ui_controller;
    public EnergyBar energyBar;

    public float runSpeed;
    public float robotMaxEnergy;
    public float idleCost;
    public float movementCost;

    public float robotEnergy;

    float decay_factor = 0.9f;

    float horizontal_target;
    float vertical_target;

    float horizontal;
    float vertical;

    public float material_amount = 0;

    Rigidbody2D body;

    // Start is called before the first frame update
    void Start()
    {
        vertical = 0;
        horizontal = 0;
        body = GetComponent<Rigidbody2D>();
        robotEnergy = robotMaxEnergy;
    }
    
    // Update is called once per frame
    void Update()
    {

        if(robotEnergy <= 0)
        {
            SceneManager.LoadScene(1);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (pz.x < transform.position.x)
            {
                this.GetComponent<Animator>().Play("PlayerActionLeft");
            }

            if (pz.x > transform.position.x)
            {
                this.GetComponent<Animator>().Play("PlayerActionRight");
            }
            
        }

        energyBar.energyPoints = robotEnergy * 100.0f / robotMaxEnergy;


        horizontal_target = Input.GetAxisRaw("Horizontal");
        vertical_target = Input.GetAxisRaw("Vertical");

        // smooth moving
        if (vertical != vertical_target)
        {
            vertical = (vertical * decay_factor + vertical_target * (1 - decay_factor));
        }


        if (horizontal != horizontal_target)
        {
            horizontal = (horizontal * decay_factor + horizontal_target * (1 - decay_factor));
        }

        if (Mathf.Abs(horizontal - horizontal_target) < 0.1f)
        {
            horizontal = horizontal_target;
        }


        if (Mathf.Abs(vertical - vertical_target) < 0.1f)
        {
            vertical = vertical_target;
        }

        //if (horizontal > 0)
        //{
        //    this.GetComponent<Animator>().Play("Left");
        //}
        //else if (horizontal < 0)
        //{
        //    this.GetComponent<Animator>().Play("Right");
        //}
        //else
        //{
        //    this.GetComponent<Animator>().Play("Idle");
        //}

    }

    private void FixedUpdate()
    {
        robotEnergy = Mathf.Clamp(robotEnergy, 0, robotMaxEnergy);

        body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);

        robotEnergy -= idleCost;
        robotEnergy -= Mathf.Abs(body.velocity.x + body.velocity.y) * movementCost;


        if (0 >= robotEnergy)
        {
            robotEnergy = 0;
            Debug.Log("You are practicly lost the game");
        }

        

    }

}
