﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public GameObject player;

    // smooth camera movement
    void FixedUpdate()
    {
        this.transform.position = new Vector3
            (
                this.transform.position.x * 0.95f + this.player.transform.position.x * 0.05f,
                this.transform.position.y * 0.95f + this.player.transform.position.y * 0.05f,
                this.transform.position.z
            );
    }
    
}
