﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundControl : MonoBehaviour
{

    public float health_level = 0f;
    public List<float> health_levels;

    Color brown = new Color(152 / 255.0f, 137 / 255.0f, 94 / 255.0f);
    Color green = new Color(149 / 255.0f, 185/ 255.0f, 96 / 255.0f);

    Color target_color;

    // Start is called before the first frame update
    void Start()
    {
        target_color = new Color(brown.r, brown.g, brown.b);
    }

    private void OnMouseDown()
    {
        GameObject.Find("ItemManager").GetComponent<ItemManager>().Deselect();
    }

    // Update is called once per frame
    void Update()
    {
        //float float_perc = Mathf.Clamp(health_level, -50, 50);
        //float_perc = (float_perc + 50) / 100.0f;

        health_level = 0;

        foreach(int i in health_levels)
        {
            health_level += i;
        }

        if (health_level > 0)
        {
            target_color = new Color(green.r, green.g, green.b);
        }
        if (health_level < 0)
        {
            target_color = new Color(brown.r, brown.g, brown.b);
        }


        Color curr_color = GetComponent<SpriteRenderer>().color;

        GetComponent<SpriteRenderer>().color = new Color(
                                                            curr_color.r * 0.99f + target_color.r * 0.01f,
                                                            curr_color.g * 0.99f + target_color.g * 0.01f,
                                                            curr_color.b * 0.99f + target_color.b * 0.01f
                                                        );
        
    }
}
