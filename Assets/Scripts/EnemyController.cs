﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float plant_detection_radius = 4;
    public float time_seconds_to_eat_tree = 5;

    Rigidbody2D body;
    float horizontal;
    float veertical;
    public float health = 100;

    bool is_eating = false;
    bool is_walking = false;
    bool reached_subject = false;
    float last_reached = 0;
    GameObject to_eat = null;
    float last_spawned = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Animator>().Play("WalkRight");
        
    }

    void Eat(GameObject to_eat_obj)
    {
        is_eating = true;
        to_eat = to_eat_obj;
        is_walking = true;
        reached_subject = false;
    }

    // Update is called once per frame
    void Update()
    {

        float t = Time.time;
        if (t - last_spawned > 2f)
        {
            ItemManager i_m = GameObject.Find("ItemManager").GetComponent<ItemManager>();
            i_m.AddGarbage( new Vector3(
                                            this.transform.position.x * Random.value * 10 - 5,
                                            this.transform.position.y * Random.value * 10 - 5,
                                            this.transform.position.z
                ) );

            last_spawned = Time.time;
        }

        if (health < 0)
        {
            Destroy(this.gameObject);
        }
        if (to_eat == null)
        {
            is_walking = false;
            is_eating = false;
            reached_subject = false;
        }

        if (to_eat == null)
        {
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, 10);
            float min_distance = 1000f;
            GameObject chosen = null;
            for (int i = 0; i < hitColliders.Length; i++)
            {
                try
                {
                    if (hitColliders[i].GetComponent<ItemSelect>().type == "Plant")
                    {
                        if (Vector3.Distance(this.transform.position, hitColliders[i].transform.position) < min_distance)
                        {
                            min_distance = Vector3.Distance(this.transform.position, hitColliders[i].transform.position);
                            chosen = hitColliders[i].gameObject;
                        }
                    }
                }
                catch
                {

                }
            }

            Eat(chosen);
        }
        if (GetComponent<Rigidbody2D>().velocity.x > 0)
        {
            GetComponent<Animator>().Play("WalkRight");
        }
        else
        {
            GetComponent<Animator>().Play("WalkLeft");
        }



    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        GetComponent<Rigidbody2D>().velocity = new Vector2(Random.value * 10 - 5, Random.value * 10 - 5);
    }

    private void FixedUpdate()
    {
        if(to_eat == null)
            transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("Player").transform.position, 0.01f);

        if (reached_subject)
        {
            GameObject parent = to_eat.GetComponent<ItemSelect>().abs_parent;
            if (Time.time - last_reached < time_seconds_to_eat_tree)
            {
            }
            else
            {
                is_eating = false;
                is_walking = false;
                reached_subject = false;
                Destroy(parent);
                to_eat = null;
            }
        }

        if (to_eat != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, to_eat.transform.position, 0.1f);
        }


        if (to_eat != null)
        {
            if (is_eating)
            {
                if (is_walking && !reached_subject)
                {
                    if (Vector3.Distance(transform.position, to_eat.transform.position) < 5f)
                    {
                        if (reached_subject == false)
                            reached_subject = true;
                        is_walking = false;
                        last_reached = Time.time;
                    }
                }

                if (reached_subject)
                {
                    GameObject parent = to_eat.GetComponent<ItemSelect>().abs_parent;
                    if (Time.time - last_reached < time_seconds_to_eat_tree)
                    {
                        //parent.GetComponentInChildren<ParticleSystem>().Play();
                        parent.transform.Rotate(Vector3.forward * (Random.value * 10 - 5));
                    }
                }

            }
        }


        if (to_eat == null)
        { 
            if (Random.value * 20 < 1)
                GetComponent<Rigidbody2D>().velocity = new Vector2(Random.value * 10 - 5, Random.value * 10 - 5);


        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        }
    }
}


