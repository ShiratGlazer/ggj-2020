﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EcologicalBar : MonoBehaviour
{
    public GameObject Fill;
    public float Points;
    public GameObject ItemManager;
    public GameObject TreesManager;
    
    [FMODUnity.EventRef]
    public string fmodEvent;

    private FMOD.Studio.EventInstance instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
        instance.start();
    }

    // Update is called once per frame
    void Update()
    {
        // Point calculation
        int good = 0, bad = 0;
        float temp;
        foreach (HealthState child in ItemManager.GetComponentsInChildren<HealthState>())
        {
            temp = child.health_factor;
            if (temp > 0)
            {
                good++;
            }
            else if(temp < 0)
            {
                bad++;
            }
            else
            {
                Debug.Log("Somthing is wrong.");
            }
        }

        foreach (HealthState child in TreesManager.GetComponentsInChildren<HealthState>())
        {
            temp = child.health_factor;
            if (temp > 0)
            {
                good++;
            }
            else if (temp < 0)
            {
                bad++;
            }
            else
            {
                Debug.Log("Somthing is wrong.");
            }
        }

        Points = good / (good + bad * 1.0f) * 100;

        // bar filling
        if (Points > 100)
        {
            Points = 100;
        }
        else if (Points < 0)
        {
            Points = 0;
        }
        Fill.transform.localScale = new Vector3(Points / 100, 1, 1);

        instance.setParameterByName("Progress", Points);
    }
}
