﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextLabelMaterialScript : MonoBehaviour
{

    public PlayerScript player_stats;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<UnityEngine.UI.Text>().text = "Material Budget: " + player_stats.material_amount;
    }
}
