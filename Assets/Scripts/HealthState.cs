﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthState : MonoBehaviour
{

    public float health_factor = -5f;
    BackgroundControl bg;


    private void Start()
    {

        bg = GameObject.FindWithTag("Background").GetComponent<BackgroundControl>();
    }

    private void OnBecameVisible()
    {
        bg.health_level += health_factor;
        bg.health_levels.Add(health_factor);
    }


    private void OnBecameInvisible()
    {
        bg.health_level -= health_factor;
        bg.health_levels.Remove(health_factor);
    }

}
