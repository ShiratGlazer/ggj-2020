﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{

    public GameObject bullet_prefab;
    public float last_shot = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, 20);

        GameObject chosen = null;
        for (int i = 0; i < hitColliders.Length; i++)
        {
            Debug.Log(hitColliders[i].gameObject.tag);
            if (hitColliders[i].gameObject.tag == "Enemies")
            {
                float t = Time.time;
                if ( t - last_shot > 1f)
                {
                    last_shot = t;
                    BulletScript bullet = Instantiate(bullet_prefab, this.transform.position, Quaternion.identity).GetComponent<BulletScript>();
                    bullet.target = hitColliders[i].gameObject;
                }
                else
                {
                    break;
                }
            }
        }
        
    }
}
